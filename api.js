const tls = require('tls')
const net = require('net')

const TLS_CLIENT_HELLO_HANDSHAKE = 0x16
const TLS_VERSION_HEADER_MAJOR = 0x03
const TLS_VERSION_HEADER_MINOR = 0x01
const TLS_HEADER = Buffer.from([
  TLS_CLIENT_HELLO_HANDSHAKE,
  TLS_VERSION_HEADER_MAJOR,
  TLS_VERSION_HEADER_MINOR
])

function pipeline (router, client, backend) {
  client.pipe(backend)
  backend.pipe(client)
  client.once('error', (error) => {
    process.nextTick(() => router.emit('clientError', error, client))
    backend.end()
  })
  backend.once('error', (error) => {
    process.nextTick(() => router.emit('clientError', error, backend))
    client.end()
  })
  client.once('end', () => {
    backend.end()
  })
  backend.once('end', () => {
    client.end()
  })
}

class TlsRouter extends net.Server {
  plaintext = undefined

  #ttfbTimeout = 10000

  #rules = []

  route (...rules) {
    for (const rule of rules) {
      this.#rules.push({
        address: rule.address,
        port: rule.port,
        family: rule.family,
        sni: rule.sni,
        alpn: rule.alpn
      })
    }
  }

  #ticketKeys = undefined

  getTicketKeys (keys) {
    return this.#ticketKeys
  }

  setTicketKeys (keys) {
    this.#ticketKeys = keys
  }

  #secureContextOptions = undefined

  setSecureContext (options) {
    this.#secureContextOptions = options
  }

  constructor (options = {}, routedConnectionListener) {
    if (!routedConnectionListener && typeof options === 'function') {
      routedConnectionListener = options
      options = {}
    }

    super(options)

    if ('ttfbTimeout' in options) {
      this.#ttfbTimeout = options.ttfbTimeout
    }

    this.on('connection', (plainSocket) => {
      const handshakeTimer = setTimeout(() => {
        plainSocket.off('readable', onReadable)
        plainSocket.end()
      }, this.#ttfbTimeout)
      const onReadable = () => {
        clearTimeout(handshakeTimer)
        const chunk = plainSocket.read()
        if (chunk === null || plainSocket.readyState !== 'open') {
          plainSocket.destroy()
        } else if (TLS_HEADER.equals(chunk.slice(0, TLS_HEADER.byteLength))) {
          const secureSocket = new tls.TLSSocket(plainSocket, {
            ticketKeys: this.getTicketKeys(),
            ...this.#secureContextOptions,
            isServer: true,
            server: this
          })
          secureSocket.once('secure', () => {
            process.nextTick(() => {
              this.emit('secureConnection', secureSocket)
            })
          })
          plainSocket.push(chunk)
        } else {
          plainSocket.push(chunk)
          process.nextTick(() => {
            this.emit('plaintextConnection', plainSocket)
          })
        }
      }
      plainSocket.once('readable', onReadable)
    })

    this.on('secureConnection', (client) => {
      const { alpnProtocol: alpn, servername: sni } = client
      for (const rule of this.#rules) {
        if (
          (!rule.sni || rule.sni === sni) &&
          (!rule.alpn || rule.alpn === alpn)
        ) {
          const address = {
            address: rule.address,
            port: rule.port,
            family: rule.family
          }
          const backend = net.createConnection(address)
          process.nextTick(() => {
            this.emit('routedConnection', rule, client, backend)
          })
          pipeline(this, client, backend)
          return
        }
      }
      if (this.listenerCount('missingRoute') > 0) {
        process.nextTick(() => {
          this.emit('missingRoute', client)
        })
      } else {
        client.end()
      }
    })

    this.on('plaintextConnection', (client) => {
      const backend = net.createConnection(this.plaintext)
      const rule = undefined
      process.nextTick(() => {
        this.emit('routedConnection', rule, client, backend)
      })
      pipeline(this, client, backend)
    })

    if (routedConnectionListener) {
      this.on('routedConnection', routedConnectionListener)
    }

    if (options.ticketKeys) {
      this.setTicketKeys(options.ticketKeys)
    }
  }
}

module.exports.TlsRouter = TlsRouter
