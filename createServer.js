const { TlsRouter } = require('./api')

function formatAddress ({ family, address }) {
  return (family === 'IPv6' || family === 6)
    ? `[${address}]` : address
}

function createServer (options) {
  const tlsRouter = new TlsRouter({ ttfbTimeout: options.ttfbTimeout })

  tlsRouter.setSecureContext({
    key: options.key,
    cert: options.cert,
    ca: options.ca,
    ALPNProtocols: options.alpn
  })

  tlsRouter.once('listening', () => {
    const { port } = tlsRouter.address()
    console.log(`Router listening at localhost:${port}`)
  })

  tlsRouter.on('routedConnection', (rule, client, backend) => {
    const { alpnProtocol: alpn, servername: sni } = client
    const from = client.address()
    const to = rule || tlsRouter.plaintext
    console.log(
      `Routing from ${formatAddress(from)} (` +
      (rule ? `TLS, ALPN=${alpn}, SNI=${sni}` : 'TCP') +
      `) to ${formatAddress(to)}:${to.port}`
    )
  })

  tlsRouter.on('missingRoute', (client) => {
    const address = formatAddress(client.address())
    const { alpnProtocol: alpn, servername: sni } = client
    console.warn(`Missing route for ${address} (ALPN=${alpn}, SNI=${sni})`)
    client.end()
  })

  tlsRouter.plaintext = options.plaintext
  tlsRouter.route(...options.routes)
  return tlsRouter
}

module.exports.createServer = createServer
