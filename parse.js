const portRegex = /:?(?<port>\d+)$/
const addressRegex = /:?(?:(?<ipv4>[\d.]+)|(?:\[(?<ipv6>[\d:a-f.]+)\])|(?<host>[-.a-z]*))$/i
const predicateRegex = /^(?:(?<alpn>[^:]*):)?(?<sni>[^:]*)?$/

function parseAddress (pattern) {
  const address = {}

  {
    const result = portRegex.exec(pattern)
    if (result) {
      const { index, groups: { port } } = result
      address.port = parseInt(port)
      pattern = pattern.substr(0, index)
    } else {
      throw new Error(`Invalid syntax, missing port in: ${pattern}`)
    }
  }

  {
    const result = addressRegex.exec(pattern)
    const { index, groups: { ipv4, ipv6, host } } = result
    if (ipv4) {
      address.address = ipv4
      address.family = 4
    } else if (ipv6) {
      address.address = ipv6
      address.family = 6
    } else {
      address.address = host || 'localhost'
      address.family = 0
    }
    pattern = pattern.substr(0, index)
  }

  return [address, pattern]
}

function parseRule (pattern) {
  const [address, predicate] = parseAddress(pattern)

  const rule = { ...address }

  {
    const result = predicateRegex.exec(predicate)
    const { groups: { alpn, sni } } = result
    rule.alpn = alpn || undefined
    rule.sni = sni || undefined
  }

  return rule
}

module.exports.parseAddress = parseAddress
module.exports.parseRule = parseRule
