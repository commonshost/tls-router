#!/usr/bin/env node

const { readFileSync } = require('fs')
const yargs = require('yargs')
const { parseRule, parseAddress } = require('./parse')
const { createServer } = require('./createServer')

const { argv } = yargs
  .env('TLS_ROUTER')
  .option('listen', {
    type: 'number',
    describe: 'Port for incoming connections'
  })
  .option('alpn', {
    type: 'array',
    describe: 'Offered ALPN names',
    default: []
  })
  .option('route', {
    type: 'array',
    describe: 'Proxy TLS clients to backend. Syntax: alpn:sni:host:port',
    default: []
  })
  .option('plaintext', {
    type: 'string',
    describe: 'Proxy TCP clients to backend. Syntax: hostname:port',
    default: ''
  })
  .option('ttfb-timeout', {
    type: 'number',
    describe: 'Maximum duration of client connection establishment',
    default: 10000
  })
  .option('public-certificate', {
    alias: 'cert',
    type: 'string',
    describe: 'File path of the TLS public certficate',
    default: ''
  })
  .option('private-key', {
    alias: 'key',
    type: 'string',
    describe: 'File path of the TLS private key',
    default: ''
  })
  .option('certificate-authority', {
    alias: 'ca',
    type: 'array',
    describe: 'File paths of the CA certificate chain',
    default: []
  })
  .usage('')
  .usage('Example: $0 --key key.pem --cert cert.pem --ca ca.pem --listen 70 --alpn gopher --route gohper:example.net:localhost:7001 --plaintext localhost:7002')
  .config()
  .version()
  .help()
  .wrap(null)

if (!argv.listen) {
  console.error('Missing option: --listen')
  process.exit(1)
}
if (!argv.publicCertificate) {
  console.error('Missing option: --public-certificate (--cert)')
  process.exit(1)
}
if (!argv.privateKey) {
  console.error('Missing option: --private-key (--key)')
  process.exit(1)
}

const options = {
  cert: readFileSync(argv.publicCertificate),
  key: readFileSync(argv.privateKey),
  ca: argv.certificateAuthority.map((ca) => readFileSync(ca)),
  ttfbTimeout: argv.ttfbTimeout,
  listen: argv.listen,
  plaintext: argv.plaintext ? parseAddress(argv.plaintext)[0] : undefined,
  alpn: argv.alpn,
  routes: argv.route.map(parseRule).filter(Boolean)
}

const server = createServer(options)

server.on('error', ({ message }) => {
  console.error(message)
  process.exit(1)
})

try {
  server.listen({ port: options.listen })
} catch (error) {
  server.emit('error', error)
}
