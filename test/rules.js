const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const net = require('net')
const { promisify } = require('util')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

const sleep = promisify(setTimeout)

async function echoIdentityServer () {
  const tcpServer = net.createServer(async (tcpClient) => {
    let received = ''
    tcpClient.on('data', (chunk) => {
      received += String(chunk)
    })
    await sleep(100)
    const response = {
      address: tcpServer.address(),
      received
    }
    tcpClient.end(JSON.stringify(response))
  })
  tcpServer.listen()
  await once(tcpServer, 'listening')
  return tcpServer
}

const scenarios = {
  both: { alpn: 'both-example', sni: 'both.example.net' },
  alpn: { alpn: 'alpn-example' },
  sni: { sni: 'sni.example.net' },
  catchall: {}
}

for (const [scenario, rule] of Object.entries(scenarios)) {
  test(`Rule matches: ${scenario}`, async (t) => {
    const tcpServer = await echoIdentityServer()

    const tlsRouter = new TlsRouter()
    tlsRouter.route({
      ...tcpServer.address(), // TCP address, family, port
      ...rule // alpn, sni
    })
    tlsRouter.setSecureContext({
      ...tlsSecureContextOptions,
      ALPNProtocols: rule.alpn ? [rule.alpn] : undefined
    })
    tlsRouter.listen()
    await once(tlsRouter, 'listening')

    const tlsClient = tls.connect({
      port: tlsRouter.address().port,
      servername: rule.sni,
      ALPNProtocols: rule.alpn ? [rule.alpn] : undefined,
      rejectUnauthorized: false
    })
    await once(tlsClient, 'connect')
    tlsClient.write('Hello, World!')

    {
      const [rule, client, backend] = await once(tlsRouter, 'routedConnection')
      t.is(rule.port, tcpServer.address().port)
      t.ok(client instanceof tls.TLSSocket)
      t.ok(backend instanceof net.Socket)
      // client.on('data', (chunk) => console.log('->', String(chunk)))
      // backend.on('data', (chunk) => console.log('<-', String(chunk)))

      let tlsClientReceivedData = ''
      tlsClient.on('data', (chunk) => {
        tlsClientReceivedData += String(chunk)
      })

      await once(tlsClient, 'close')
      const response = JSON.parse(tlsClientReceivedData)
      t.deepEqual(response.address, tcpServer.address())
      t.is(response.received, 'Hello, World!')
    }

    tcpServer.close()
    await once(tcpServer, 'close')

    tlsRouter.close()
    await once(tlsRouter, 'close')
  })
}
