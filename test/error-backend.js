const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

test('Backend not available', async (t) => {
  const tlsRouter = new TlsRouter()
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.route({ port: 0 })
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    port: tlsRouter.address().port,
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')
  const [error] = await once(tlsRouter, 'clientError')
  t.ok([
    'ECONNREFUSED', // Linux
    'EADDRNOTAVAIL' // MacOS
  ].includes(error.code))
  await once(tlsClient, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
