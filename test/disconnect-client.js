const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const net = require('net')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

test('Client disconnects', async (t) => {
  const tcpServer = net.createServer()
  tcpServer.listen()
  await once(tcpServer, 'listening')

  const tlsRouter = new TlsRouter()
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.route(tcpServer.address())
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    port: tlsRouter.address().port,
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')
  tlsClient.write('Hello, World!')
  tlsClient.on('data', console.log)

  const [rule, client] = await once(tlsRouter, 'routedConnection')
  t.ok(rule)
  t.ok(client)

  const [tcpClient] = await once(tcpServer, 'connection')
  tcpClient.resume()

  tlsClient.destroy()

  await Promise.all([
    once(tcpClient, 'close'),
    once(tlsClient, 'close')
  ])

  tcpServer.close()
  await once(tcpServer, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
