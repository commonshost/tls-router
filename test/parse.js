const test = require('blue-tape')
const { parseRule, parseAddress } = require('../parse.js')

const fixtures = {
  rules: [
    {
      given: 'gopher:example.net:localhost:7000',
      expected: {
        sni: 'example.net',
        alpn: 'gopher',
        port: 7000,
        address: 'localhost',
        family: 0
      }
    },
    {
      given: 'gopher:example.net:127.0.0.1:7000',
      expected: {
        sni: 'example.net',
        alpn: 'gopher',
        port: 7000,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: 'gopher:example.net:[::1]:7000',
      expected: {
        sni: 'example.net',
        alpn: 'gopher',
        port: 7000,
        address: '::1',
        family: 6
      }
    },
    {
      given: ':example.net:127.0.0.1:7000',
      expected: {
        sni: 'example.net',
        alpn: undefined,
        port: 7000,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: 'gopher::127.0.0.1:7000',
      expected: {
        sni: undefined,
        alpn: 'gopher',
        port: 7000,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: '7000',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7000,
        address: 'localhost',
        family: 0
      }
    },
    {
      given: ':7000',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7000,
        address: 'localhost',
        family: 0
      }
    },
    {
      given: '127.0.0.1:7000',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7000,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: '[::1]:7000',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7000,
        address: '::1',
        family: 6
      }
    },
    {
      given: ':example.net:127.0.0.1:7000',
      expected: {
        sni: 'example.net',
        alpn: undefined,
        port: 7000,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: '127.0.0.1:7777',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7777,
        address: '127.0.0.1',
        family: 4
      }
    },
    {
      given: '[::1]:7777',
      expected: {
        sni: undefined,
        alpn: undefined,
        port: 7777,
        address: '::1',
        family: 6
      }
    }
  ],
  addresses: [
    {
      given: 'localhost:7777',
      expected: {
        port: 7777,
        address: 'localhost',
        family: 0
      }
    }
  ]
}

for (const { given, expected } of fixtures.rules) {
  test(`Parsing rule: ${given}`, async (t) => {
    const actual = parseRule(given)
    t.deepEqual(actual, expected)
  })
}

for (const { given, expected } of fixtures.addresses) {
  test(`Parsing address: ${given}`, async (t) => {
    const [actual] = parseAddress(given)
    t.deepEqual(actual, expected)
  })
}

test('Throw if missing port', async (t) => {
  const given = 'rule-without-port'
  t.throws(
    () => parseRule(given),
    /^Error: Invalid syntax, missing port in: /
  )
  t.throws(
    () => parseAddress(given),
    /^Error: Invalid syntax, missing port in: /
  )
})
