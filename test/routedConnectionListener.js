const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const net = require('net')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

test('Optional constructor callback argument', async (t) => {
  const tcpServer = net.createServer((client) => client.resume())
  tcpServer.listen()
  await once(tcpServer, 'listening')

  let resolver
  const done = new Promise((resolve) => { resolver = resolve })
  const tlsRouter = new TlsRouter((...args) => resolver(args))
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.route(tcpServer.address())
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    ...tlsRouter.address(),
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')
  tlsClient.resume()
  tlsClient.write('Hello, World!')

  const [rule, client, backend] = await done
  t.deepEqual(rule, { ...tcpServer.address(), sni: undefined, alpn: undefined })
  t.ok(client instanceof net.Socket)
  t.ok(backend instanceof net.Socket)
  t.isNot(client, backend)

  tlsClient.end()
  await once(tlsClient, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')

  tcpServer.close()
  await once(tcpServer, 'close')
})
