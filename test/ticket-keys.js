const test = require('blue-tape')
const { TlsRouter } = require('..')
const { randomBytes } = require('crypto')

test('Set/get ticket keys using instance methods', async (t) => {
  const tlsRouter = new TlsRouter()
  const ticketKeys = randomBytes(48)
  tlsRouter.setTicketKeys(ticketKeys)
  const actual = tlsRouter.getTicketKeys()
  t.is(actual, ticketKeys)
})

test('Set ticket keys using constructor options', async (t) => {
  const ticketKeys = randomBytes(48)
  const options = { ticketKeys }
  const tlsRouter = new TlsRouter(options)
  const actual = tlsRouter.getTicketKeys()
  t.is(actual, ticketKeys)
})
