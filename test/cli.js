const test = require('blue-tape')
const { join } = require('path')
const { fork } = require('child_process')
const tls = require('tls')
const net = require('net')
const { once } = require('events')

const modulePath = join(__dirname, '../cli.js')

test('Run the CLI', async (t) => {
  let receivedPlaintext = ''
  const tcpServerPlaintext = net.createServer(async (client) => {
    client.write('Good Bye!')
    for await (const chunk of client) {
      receivedPlaintext += chunk.toString()
    }
  })
  tcpServerPlaintext.listen()
  await once(tcpServerPlaintext, 'listening')

  let receivedBackend = ''
  const tcpServerBackend = net.createServer(async (client) => {
    client.write('Good Bye!')
    for await (const chunk of client) {
      receivedBackend += chunk.toString()
    }
  })
  tcpServerBackend.listen()
  await once(tcpServerBackend, 'listening')

  const routerPort = 7000
  const alpn = 'foobar'
  const sni = 'example.net'

  const args = [
    '--listen', routerPort,
    '--alpn', alpn,
    '--cert', 'cert.pem',
    '--key', 'key.pem',
    '--plaintext', `localhost:${tcpServerPlaintext.address().port}`,
    '--route', `${alpn}:${sni}:localhost:${tcpServerBackend.address().port}`,
    '--route', `${alpn}:ipv4.${sni}:127.0.0.1:${tcpServerBackend.address().port}`,
    '--route', `${alpn}:ipv6.${sni}:[::1]:${tcpServerBackend.address().port}`
  ]
  const options = {
    stdio: ['inherit', 'pipe', 'pipe', 'ipc']
  }
  const child = fork(modulePath, args, options)
  child.stderr.on('data', (chunk) => {
    process.stderr.write('CLI STDERR: ' + chunk.toString())
  })
  child.stdout.on('data', (chunk) => {
    process.stdout.write('CLI STDOUT: ' + chunk.toString())
  })
  await once(child.stdout, 'data')

  for (const tcpVersion of [0, 4, 6]) {
    for (const sniPrefix of ['', 'ipv4.', 'ipv6.']) {
      receivedBackend = '' // Reset buffer
      const tlsClient = tls.connect({
        port: routerPort,
        rejectUnauthorized: false,
        servername: sniPrefix + sni,
        ALPNProtocols: [alpn],
        family: tcpVersion
      })
      await once(tlsClient, 'connect')
      tlsClient.write('Hello, World!')
      const [responseBackend] = await once(tlsClient, 'data')
      t.is(responseBackend.toString(), 'Good Bye!')
      tlsClient.end()
      await once(tlsClient, 'close')
      t.is(receivedBackend, 'Hello, World!')
    }
  }

  {
    const tlsClient = tls.connect({
      port: routerPort,
      rejectUnauthorized: false,
      servername: 'does-not-exist.' + sni,
      ALPNProtocols: ['does-not-exist']
    })
    await once(tlsClient, 'connect')
    tlsClient.write('Hello, World!')
    const [chunk] = await once(child.stderr, 'data')
    t.ok(chunk.toString().includes('Missing route'))
    await once(tlsClient, 'close')
  }

  {
    receivedPlaintext = '' // Reset buffer
    const tcpClient = net.connect({
      port: routerPort
    })
    await once(tcpClient, 'connect')
    tcpClient.write('Hello, World!')
    const [responsePlaintext] = await once(tcpClient, 'data')
    t.is(responsePlaintext.toString(), 'Good Bye!')
    tcpClient.end()
    await once(tcpClient, 'close')
    t.is(receivedPlaintext, 'Hello, World!')
  }

  child.kill()
  const [code] = await once(child, 'exit')
  t.is(code, null)

  tcpServerPlaintext.close()
  await once(tcpServerPlaintext, 'close')
  tcpServerBackend.close()
  await once(tcpServerBackend, 'close')
})

for (const { label, args } of [
  {
    label: '--listen',
    args: []
  },
  {
    label: '--public-certificate',
    args: [
      '--listen', 1234
    ]
  },
  {
    label: '--private-key',
    args: [
      '--listen', 1234,
      '--cert', 'cert.pem'
    ]
  }
]) {
  test(`Missing CLI option: ${label}`, async (t) => {
    const options = {
      stdio: ['inherit', 'inherit', 'pipe', 'ipc']
    }
    const child = fork(modulePath, args, options)
    const [chunk] = await once(child.stderr, 'data')
    process.stderr.write(chunk)
    t.ok(chunk.toString().includes(`Missing option: ${label}`))
    const [code] = await once(child, 'exit')
    t.is(code, 1)
  })
}

test('Multiple CA', async (t) => {
  const options = {
    stdio: ['inherit', 'pipe', 'inherit', 'ipc']
  }
  const args = [
    '--listen', 4321,
    '--key', 'key.pem',
    '--cert', 'cert.pem',
    '--ca', 'key.pem', 'cert.pem'
  ]
  const child = fork(modulePath, args, options)
  const [chunk] = await once(child.stdout, 'data')
  process.stdout.write(chunk)
  t.ok(chunk.toString().startsWith('Router listening'))
  child.kill()
  const [code] = await once(child, 'exit')
  t.is(code, null)
})

test('Server throws error', async (t) => {
  const options = {
    stdio: 'inherit'
  }
  const args = [
    '--listen', -1,
    '--key', 'key.pem',
    '--cert', 'cert.pem',
    '--ca', 'key.pem', 'cert.pem'
  ]
  const child = fork(modulePath, args, options)
  const [code] = await once(child, 'exit')
  t.is(code, 1)
})
