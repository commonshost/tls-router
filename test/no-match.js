const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const net = require('net')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

test('No matching route emits missingRoute event', async (t) => {
  const tlsRouter = new TlsRouter()
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.listen()
  tlsRouter.route(
    { sni: 'does-not-exist.example.net' },
    { alpn: 'does-not-exist' }
  )
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    port: tlsRouter.address().port,
    servername: 'no-match.example.net',
    ALPNProtocols: ['no-match'],
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')

  const [client] = await once(tlsRouter, 'missingRoute')
  t.ok(client instanceof net.Socket)
  t.is(client.destroyed, false)
  client.destroy()
  await Promise.all([
    once(tlsClient, 'close'),
    once(client, 'close')
  ])

  tlsRouter.close()
  await once(tlsRouter, 'close')
})

test('No matching route ends client without event handler', async (t) => {
  const tlsRouter = new TlsRouter()
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.listen()
  tlsRouter.route(
    { sni: 'does-not-exist.example.net' },
    { alpn: 'does-not-exist' }
  )
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    port: tlsRouter.address().port,
    servername: 'no-match.example.net',
    ALPNProtocols: ['no-match'],
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')

  await once(tlsClient, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
