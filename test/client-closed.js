const test = require('blue-tape')
const { TlsRouter } = require('..')
const net = require('net')
const { once } = require('events')

test('TCP client port scanning', async (t) => {
  const tlsRouter = new TlsRouter()
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tcpClient = net.connect(tlsRouter.address())
  await once(tcpClient, 'connect')
  tcpClient.end()
  await once(tcpClient, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
