const test = require('blue-tape')
const { TlsRouter } = require('..')
const net = require('net')
const { once } = require('events')

test('Plaintext client connection', async (t) => {
  let backendReceived = ''
  const tcpServer = net.createServer(async (client) => {
    client.write('Goodbye.')
    for await (const chunk of client) {
      backendReceived += chunk.toString()
    }
  })
  tcpServer.listen()
  await once(tcpServer, 'listening')

  const tlsRouter = new TlsRouter()
  tlsRouter.plaintext = tcpServer.address()
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tcpClient = net.createConnection(tlsRouter.address())
  await once(tcpClient, 'connect')
  tcpClient.write('Hello, World!')

  const [received] = await once(tcpClient, 'data')
  t.is(received.toString(), 'Goodbye.')

  tcpClient.end()
  await once(tcpClient, 'close')

  t.is(backendReceived, 'Hello, World!')

  tlsRouter.close()
  await once(tlsRouter, 'close')

  tcpServer.close()
  await once(tcpServer, 'close')
})
