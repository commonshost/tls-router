const test = require('blue-tape')
const { TlsRouter } = require('..')
const tls = require('tls')
const net = require('net')
const { once } = require('events')
const { tlsSecureContextOptions } = require('./helpers')

test('Client errors propagate to router', async (t) => {
  const tcpServer = net.createServer()
  tcpServer.listen()
  await once(tcpServer, 'listening')

  const tlsRouter = new TlsRouter()
  tlsRouter.setSecureContext(tlsSecureContextOptions)
  tlsRouter.route(tcpServer.address())
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tlsClient = tls.connect({
    port: tlsRouter.address().port,
    rejectUnauthorized: false
  })
  await once(tlsClient, 'connect')
  tlsClient.resume()

  const [, tlsRouterClient] = await once(tlsRouter, 'routedConnection')

  const [tcpClient] = await once(tcpServer, 'connection')
  tcpClient.resume()

  tlsRouterClient.emit('error', new Error('Spoofed'))
  const [error, client] = await once(tlsRouter, 'clientError')
  t.ok(error instanceof Error)
  t.ok(client instanceof net.Socket)

  await Promise.all([
    once(tcpClient, 'close'),
    once(tlsClient, 'close')
  ])

  tcpServer.close()
  await once(tcpServer, 'close')

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
