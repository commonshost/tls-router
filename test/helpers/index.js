const fs = require('fs')

const tlsSecureContextOptions = {
  ecdhCurve: 'P-384:P-256',
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  ca: [/* fs.readFileSync('ca.pem') */]
}

module.exports = {
  tlsSecureContextOptions
}
