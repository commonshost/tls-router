const test = require('blue-tape')
const { TlsRouter } = require('..')
const net = require('net')
const { once } = require('events')

test('TCP client handshake timeout', async (t) => {
  const ttfbTimeout = 100
  const tlsRouter = new TlsRouter({ ttfbTimeout })
  tlsRouter.listen()
  await once(tlsRouter, 'listening')

  const tcpClient = net.connect(tlsRouter.address())
  await once(tcpClient, 'connect')
  const begin = Date.now()
  // TTFB timing out...
  await once(tcpClient, 'close')
  const end = Date.now()
  const elapsed = end - begin
  t.ok(elapsed >= ttfbTimeout)

  tlsRouter.close()
  await once(tlsRouter, 'close')
})
